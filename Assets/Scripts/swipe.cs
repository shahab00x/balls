﻿using UnityEngine;
using System.Collections;

public class swipe : MonoBehaviour
{
    public GameObject limit_line;

    private bool collided = false;
    private bool too_high = false;

    private float dist;
    private Vector3 v3Offset;
    private Plane plane;
    private Rigidbody2D rb;

    private Vector2 lastPosition;
    private float lastTime;

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {       
        if (lastTime - Time.time > .05)
        {
            lastPosition = transform.position;
            lastTime = Time.time;
        }

        if (too_high == false)
        {
            RaycastHit hit = new RaycastHit();
            for (int i = 0; i < Input.touchCount; ++i)
            {
                if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
                {
                    // Construct a ray from the current touch coordinates
                    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
                    if (Physics.Raycast(ray, out hit))
                    {
                        hit.transform.gameObject.SendMessage("OnMouseDown");
                    }

                }
            }
        }
    }

    void OnMouseDown()
    {
        if (too_high == true)
        {
            return;
        }

        lastPosition = transform.position;
        lastTime = Time.time;

        plane.SetNormalAndPosition(Camera.main.transform.forward, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dist;
        plane.Raycast(ray, out dist);
        v3Offset = transform.position - ray.GetPoint(dist);
    }
    
    void OnMouseDrag()
    {
        if (this.transform.position.y >= limit_line.transform.position.y)
        {
            release_ball();
            too_high = true;
            return;
        }

        if (collided == true)
        {
            collided = false;
            return;
        }
        if (too_high == true) return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float dist;
        plane.Raycast(ray, out dist);
        Vector3 v3Pos = ray.GetPoint(dist);
        transform.position = v3Pos + v3Offset;
        //   lastPosition = transform.position;
        // lastTime = Time.time;

    }

    void OnMouseUp()
    {
        if (too_high == false)
        {
            release_ball();
        } else
        {
            too_high = false;
        }
    }

    void release_ball()
    {
        if (too_high == false)
        {
            // rb.AddForceAtPosition(new Vector2(2, 10)*4, transform.position, ForceMode2D.Impulse);
            Vector2 pos = transform.position;
            Vector2 vel = (pos - lastPosition) / (Time.time - lastTime);
            rb.velocity = vel;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "edge" )
            collided = true;
        else
            collided = false;

      //  Debug.Log(other.contacts.Length);
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "edge")
            collided = false;
    }

    void OnCollisionStay2D(Collision2D other)
    {
        
       
    }
}
