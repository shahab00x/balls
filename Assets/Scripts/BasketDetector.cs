﻿using UnityEngine;
using System.Collections;

public class BasketDetector : MonoBehaviour {
    public AudioClip score_sound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
    {
        AudioSource.PlayClipAtPoint(score_sound, transform.position);
        Debug.Log("Trigger");
        Rigidbody2D gor = other.gameObject.GetComponent<Rigidbody2D>();
        float speed = gor.velocity.y;
        if (speed < 0)
        {
            ScoreManager.AddScore(1);
            AudioSource.PlayClipAtPoint(score_sound, transform.position);
        }
    }
}
