﻿using UnityEngine;
using System.Collections;

public class LimitLineScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerExit2D(Collider2D other)
    {
        // Destroy(other.gameObject);
        if (other.gameObject.tag == "draggable")
            if ( other.gameObject.transform.position.y < this.transform.position.y)
            {
                Camera camera = FindObjectOfType<Camera>();
                Vector3 p;
                p.x = Screen.width/2 ;
                p.y = Screen.height/10;
                p.z = 0;
                p = camera.ScreenToWorldPoint(p);
                p.z = other.gameObject.transform.position.z;

                other.gameObject.transform.position = p;
                Rigidbody2D rb = other.gameObject.GetComponent<Rigidbody2D>();
                rb.velocity = new Vector2(0,0);
                rb.angularVelocity = 0;
            }

    }
}
