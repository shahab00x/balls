﻿using UnityEngine;
using System.Collections;

public class controlCamera : MonoBehaviour {
	public Transform target;
	// Use this for initialization
	void Start () {
	}

	void Update () {

	}

	// LateUpdate is called once per frame
	void LateUpdate () {
		if (target.position.y < 0)
			transform.position = new Vector3 (transform.position.x, 0, transform.position.z);
		else if(target.position.y > 19)
			transform.position = new Vector3 (transform.position.x, 19, transform.position.z);
		else
			transform.position = new Vector3 (transform.position.x, target.position.y, transform.position.z);
	}
}
