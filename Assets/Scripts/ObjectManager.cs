﻿using UnityEngine;
using System.Collections;

public class ObjectManager : MonoBehaviour {

    public static GameObject BallObject;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public static void SpawnBall()
    {
        SpawnObjectAt(new Vector2(Screen.width / 2, Screen.height / 10), BallObject);
    }

    private static void SpawnObjectAt(Vector2 position, GameObject myObject)
    {
        Vector3 pos = new Vector3(position.x, position.y, 0);
        
        GameObject obj = Instantiate(myObject);
        obj.transform.position = pos; 
    }
}
